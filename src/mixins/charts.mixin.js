/* eslint-disable */
import tip from 'd3-tip';
import * as d3 from 'd3';
import moment from 'moment';
import { cloneDeep } from 'lodash/lang';
import { chunk, findIndex } from 'lodash/array';

export default ({
  watch: {
    chartData() { window.clearTimeout(this.slideTimeout); this.slideTimeout = window.setTimeout(() => this.updateData(), 350) },
    selectedIndex() { window.clearTimeout(this.slideTimeout); this.slideTimeout = window.setTimeout(() => this.updateData(), 350) },
  },
  computed: {
    sortedData() {
      return cloneDeep(this.chartData).sort((a, b) => moment(a.TIMESTAMP).isSameOrBefore(moment(b.TIMESTAMP)) ? -1 : 1);
    },
    totalDays() {
      return chunk(this.sortedData.map(d => new Date(d.TIMESTAMP)), this.selectionLength);
    },
    xLabels() {
      return this.sortedData.map(d => new Date(d.TIMESTAMP)).slice(this.startIndex, this.endIndex);
    },
    xLabelsISO() {
      return this.xLabels.map(d => d.toISOString()).slice(this.startIndex, this.endIndex);
    },
    xLabelsISOTotal() {
      return this.sortedData.map(d => new Date(d.TIMESTAMP).toISOString()); 
    },
    yData() {
      return this.sortedData.map(d => d.data).slice(this.startIndex, this.endIndex);
    },
    width() {
      return this.$el.offsetWidth - (this.margin.left + this.margin.right);
    },
    height() {
      return this.$el.offsetHeight - (this.margin.top + this.margin.bottom);
    },
    endIndex() {
      const nIndex = this.selectedIndex + this.selectionLength;
      return nIndex < this.chartData.length ? nIndex : this.chartData.length;
    },
    startIndex() {
      const nIndex = this.endIndex - this.selectionLength;
      return nIndex >= 0 ? nIndex : 0;
    },
  },
  methods: {
    startSliding(day) {
      if (!this.isSliding) {
        this.slideClickStartIndex = this.xLabelsISOTotal.indexOf(day);
        this.slideStartIndex = this.startIndex;
        this.isSliding = true;
      }
    },
    slideTo(day) {
      if (this.isSliding && this.yData.length > 1) {
        const indexOfDay = this.xLabelsISOTotal.indexOf(day);
        const distance = this.slideClickStartIndex - indexOfDay;
        const nIndex = this.slideStartIndex - distance;
        this.selectedIndex = nIndex >= 0 ? nIndex : 0;
      }
    },
    moveTo(day) {
      const index = this.xLabelsISOTotal.indexOf(day);
      if (index >= 0) this.selectedIndex = index;
    },
    createGraph() {
      this.chartContainer = d3.select(this.$el.querySelector('svg.chart-container__chart'));
      this.chart = this.chartContainer.append("g")
                                      .attr("transform", `translate(0, ${-this.margin.bottom})`);
    },
    createScales() {
      this.scales.x = d3.scaleBand()
                        .domain(this.xLabels)
                        .rangeRound([this.margin.left, this.width])
                        .padding(0.1);

      this.scales.y = d3.scaleLinear()
                        .domain([d3.max(this.yData), 0])
                        .rangeRound([this.margin.bottom, (this.height + this.margin.top)])
                        .nice(1);
    },
    createAxis() {
      this.axis.x = d3.axisBottom(this.scales.x).tickFormat(d3.timeFormat("%d/%m/%Y"));
      this.axis.y = d3.axisLeft(this.scales.y).ticks(10);

      this.fakeAxis.x = d3.axisBottom(this.scales.x)
      .tickSize(-this.height)
      .tickFormat("");
      this.fakeAxis.y = d3.axisLeft(this.scales.y)
      .tickSize(-(this.width-this.margin.left-this.margin.right), 0, 0)
      .tickFormat("");

      this.chart.append('g')
                .attr('class', 'axis axis--x')
                .attr('transform', `translate(0, ${this.height + this.margin.top})`)
                .call(this.axis.x);

      this.chart.append('g')
                .attr('class', 'axis axis--y')
                .attr('transform', `translate(${this.margin.left}, 0)`)
                .call(this.axis.y);
    },
    drawLegend() {
      const self = this;
      this.legend = this.chart.append("g")
                    .attr("class", "legend")
                    .attr("x", this.width - 65)
                    .attr("y", 25)
                    .attr("height", 100)
                    .attr("width", 100);

      this.legend.selectAll('g').data(['element 1'])
        .enter()
        .append('g')
        .each(function(d, i) {
          var g = d3.select(this);
          g.append("rect")
            .attr("x", self.width - 50)
            .attr("y", 60 + i*25)
            .attr("width", 10)
            .attr("height", 10)
            .style("fill", 'black');
          
          g.append("text")
            .attr("x", self.width - 32)
            .attr("y", 60 + (i * 25) + 10)
            .attr("height",30)
            .attr("width",100)
            .style("fill", 'black')
            .text(d => d);

        });
    },
    drawGrid() {
      // Vertical grid
      if (this.grid.vertical) {
        this.chart
            .append('g')
            .attr("class", "grid grid--v")
            .attr("transform", `translate(0, ${this.height + this.margin.top})`)
            .call(this.fakeAxis.x);
      }
        
      // Horizontal grid
      if (this.grid.horizontal) {
        this.chart
            .append('g')
            .attr("class", "grid grid--h")
            .attr('transform', `translate(${this.margin.left}, 0)`)
            .call(this.fakeAxis.y);
      }
    },
    formatDate(date) {
      return moment(date).format('D/MM/YYYY');
    }
  },
})